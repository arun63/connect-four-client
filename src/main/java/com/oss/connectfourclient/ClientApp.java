package com.oss.connectfourclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oss.connectfourclient.domain.GameState;
import com.oss.connectfourclient.domain.Player;
import com.oss.connectfourclient.domain.Status;
import com.oss.connectfourclient.domain.UpdateBoard;
import com.oss.connectfourclient.request.ServerRequest;
import com.oss.connectfourclient.utils.GameUtils;

import java.io.IOException;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * The Client Application Main Class.
 */
public class ClientApp {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static void main(String[] args) throws IOException, InterruptedException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the player name");
        String playerName = scanner.nextLine();
        System.out.println("Select your symbol (X or O)");
        String symbol = scanner.nextLine();
        GameState game = joinGame(playerName, symbol);
        Boolean isPlay = true;
        while (isPlay) {
            game = getGameStatus(game.getGameId(), game);
            if (game.getStatus().equals(Status.ONE_PLAYER_CONNECTED)) {
                System.out.println("Waiting for another player to join.");
            } else if (game.getStatus().equals(Status.COMPLETED)) {
                System.out.println("The game is over. The winner is " + game.getWinner());
                break;
            }
            if (game.getValue() != null && game.getValue().equals(symbol)) {
                GameUtils.printBoard(game.getBoard());
                System.out.println("It's your turn " + playerName + ", please enter column (1-9)");
                int value = Integer.parseInt(scanner.nextLine());
                while (value < 0 || value > GameUtils.COLUMNS) {
                    System.out.println("You must enter a valid number between 1 and 9: Try Again");
                    value = Integer.parseInt(scanner.nextLine());
                }
                game = updateGame(game.getGameId(), value, symbol, game);
                if (game.getStatus().equals(Status.COLUMN_EXISTS)) {
                    System.out.println("All the column is full. You value has been skipped");
                }
                GameUtils.printBoard(game.getBoard());
                if (game.getStatus().equals(Status.COMPLETED)) {
                    System.out.println("Game Over. The winner is " + game.getWinner());
                    isPlay = false;
                }
            } else {
                TimeUnit.SECONDS.sleep(3);
            }
        }
    }

    private static GameState joinGame(String playerName, String symbol) throws IOException {
        GameState game = new GameState();
        Player player = new Player();
        player.setPlayerName(playerName);
        player.setSymbol(symbol);
        String requestBody = objectMapper.writeValueAsString(player);
        String response = ServerRequest.postRequest(requestBody);
        if (response != null) {
            game = objectMapper.readValue(response, GameState.class);
            System.out.println("You have successfully joined a game");
        }
        return game;
    }

    private static GameState updateGame(UUID gameId, int value, String symbol, GameState game) throws IOException {
        UpdateBoard updateBoard = new UpdateBoard();
        updateBoard.setValue(value);
        updateBoard.setSymbol(symbol);
        String requestBody = objectMapper.writeValueAsString(updateBoard);
        String response = ServerRequest.patchRequest(gameId.toString(), requestBody);
        if (response != null) {
            game = objectMapper.readValue(response, GameState.class);
        }
        return game;
    }

    private static GameState getGameStatus(UUID gameId, GameState game) throws IOException {
        String response = ServerRequest.getRequest(gameId.toString());
        if (response != null) {
            game = objectMapper.readValue(response, GameState.class);
        }
        return game;
    }
}
