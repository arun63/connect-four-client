package com.oss.connectfourclient.request;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * The server request builder class.
 */
public class ServerRequest {

    private static URL url;

    static {
        try {
            url = new URL(getServerUri() + "/v1/api/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private static String getServerUri() {
        return System.getProperty("serverAddress", "http://localhost:8080");
    }

    /**
     * The POST request to the server.
     *
     * @param requestBody the request payload.
     * @return response string.
     * @throws IOException input/output exception.
     */
    public static String postRequest(String requestBody) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(requestBody, mediaType);
        Request request = new Request.Builder()
                .url(url + "join")
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful() && response.body() != null) {
            return response.body().string();
        }
        return null;
    }

    /**
     * The GET request to the server.
     *
     * @param gameId the game identifier.
     * @return the response string.
     * @throws IOException input/output exception.
     */
    public static String getRequest(String gameId) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(url + gameId)
                .method("GET", null)
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful() && response.body() != null) {
            return response.body().string();
        }
        return null;
    }

    /**
     * The PATCH request to the server.
     *
     * @param gameId      the game identifier.
     * @param requestBody the request payload.
     * @return the response string.
     * @throws IOException input/output exception.
     */
    public static String patchRequest(String gameId, String requestBody) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(1000, TimeUnit.MINUTES)
                .writeTimeout(1000, TimeUnit.MINUTES)
                .readTimeout(1000, TimeUnit.MINUTES)
                .build();

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(requestBody, mediaType);
        Request request = new Request.Builder()
                .url(url + gameId)
                .method("PATCH", body)
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful() && response.body() != null) {
            return response.body().string();
        }
        return null;
    }
}
