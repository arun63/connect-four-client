package com.oss.connectfourclient.domain;

import java.io.Serializable;

/**
 * Domain Model for Player Object.
 * It consists of name and the symbol (X or O).
 */
public class Player implements Serializable {

    private String playerName;
    private String symbol;

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String name) {
        this.playerName = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
