package com.oss.connectfourclient.utils;

/**
 * Utility Game Service Class.
 */
public class GameUtils {

    public static final int ROWS = 6;
    public static final int COLUMNS = 9;
    public static final int WIN_COUNT = 5;

    public static void printBoard(String[][] board) {
        for (int i = 0; i < GameUtils.ROWS; i++) {
            System.out.print("|");
            for (int z = 0; z < GameUtils.COLUMNS; z++) {
                if (board[i][z] == null) {
                    board[i][z] = "[ ]";
                }
                System.out.print(board[i][z]);
            }
            System.out.print("| \n");
        }
        System.out.println("________________________________");
    }
}
