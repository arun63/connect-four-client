[![build status](https://gitlab.com/arun63/connect-four-client/badges/development/pipeline.svg)](https://gitlab.com/arun63/connect-four-client})

# Connect Four Client - Plain Text

> A simple plain text version of famous connect four game (5-in-a-row) java client that provides consumes
> the end point from the Connect Four Server (Client-Server Architecture). The service is containerized using docker.
---
### Tech Stack

- [Java 11](https://www.oracle.com/ie/java/technologies/javase-jdk11-downloads.html)
- [JUnit](https://junit.org/junit5/docs/current/user-guide/)
- [Docker](https://www.docker.com/)
- [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/)

---

### Pre-Requisites
- Download the Connect Four Server Project and follow the steps to run it.
```sh
git clone git@gitlab.com:arun63/connect-four-server.git
```

### Commands
- #### local installation
  After running the server, the client is build and run through gradle command.
  ```sh
  gradle clean build && gradle run
  ```
- #### Run the Jar
  You can run the jar into multiple terminal.
  ```sh
  java -jar {$PROJECT_DIR}/build/libs/connect-four-client-0.0.1-SNAPSHOT.jar
  ```
- #### Docker 
  Alternatively, use docker for building and running the application
  ```sh
  docker build -t connect-four-client .
  ```
  To run the docker image
  ```sh
  docker run connect-four-client
  ```

### Continuous Integration

3 stages of pipelines on each commit
```
 - compile  # compiles - gradle compile
 - qa       # code quality analysis - it runs the checkstyle
 - build-container # docker containization for the deployment
```
The docker container is pushed into the docker registry.


### Approach
- Client-Server Architecture and is termed as stand-alone, production-grade hierarchical structure.
- Java 11 as the preferred language and junit as the test framework. 
- Client receives and display the input and output through standard input (stdin).
- The application has continuous polls to the server to get the game status.
- Docker is used as the preferred containerization and the gitlab for the ci/cd infrastructure

### Branch

Creating a new branch based on the feature, bugfix, hotfix, security
- `feature/<feature-name>` - The product incremental feature
- `bugfix/<bugfix-name>` - The bugfix involved in various stages and feature
- `hotfix/<hotfix-name>` - Immediate fix that requires immediate merge
- `security/<security-name>` - security related features and fixes.

`Note`: Pipeline will only trigger if it follows branching strategy